﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using GameTools;
using MyFirstProgram;
namespace GameEnviroment
{
    class MatrixGame : GameEngine
    {
        private char[] characters = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'n'};
        private MatrixRepresentation matrix;
        private Random rnumber = new Random();
        private Boolean falling = false;
        private ConsoleKeyInfo teclat;
        public MatrixGame():base()
        {
            
        }
        protected override void Start()
        {
            this.matrix = new MatrixRepresentation();
            matrix.CleanTheMatrix();
            Console.ForegroundColor = ConsoleColor.Green;
        }
        protected override void Update()
        {
            //Execution ontime secuence of every frame
            moveValues();
            matrix.printMatrix();
            if (!falling)
            {
                Console.WriteLine("Prem qualsevol tecla per canviar a mode selecció de columnes.");
            }
            else
            {
                Console.WriteLine("Prem el valor de la columna per on vol que caiguin els caracters.\nPer sortir prem el 0");
            }
            checkKeyboard();
        }

        protected override void Exit()
        {
            //Code afer last frame
        }
        private void moveValues()
        {
            char[,] novaMatriu = new char[matrix.TheMatrix.GetLength(0), matrix.TheMatrix.GetLength(1)];
            for (int x = novaMatriu.GetLength(0) - 1; x >= 0; x--)
            {
                for (int y= 0; y < novaMatriu.GetLength(1); y++)
                {
                    checkKeyboard();
                    if (x != 0)
                    {
                        novaMatriu[x, y] = matrix.TheMatrix[x - 1, y];
                    }
                    else
                    {
                        if (!falling)
                        {
                            novaMatriu[x, y] = randomChar();
                        }
                        else
                        {
                            int values=9;
                            if (cki.Key == ConsoleKey.D1)
                            {
                                values = 0;
                            }else if (cki.Key == ConsoleKey.D2)
                            {
                                values = 1;
                            }else if (cki.Key == ConsoleKey.D3)
                            {
                                values = 2;
                            }
                            else if (cki.Key == ConsoleKey.D4)
                            {
                                values = 3;
                            }
                            else if (cki.Key == ConsoleKey.D5)
                            {
                                values = 4;
                            }
                            else if (cki.Key == ConsoleKey.D6)
                            {
                                values = 5;
                            }
                            else if (cki.Key == ConsoleKey.D7)
                            {
                                values = 6;
                            }
                            else if (cki.Key == ConsoleKey.D8)
                            {
                                values = 7;
                            }
                            else if (cki.Key == ConsoleKey.D9)
                            {
                                values = 8;
                            }
                            else if (cki.Key == ConsoleKey.D0)
                            {
                                falling = false;
                            }
                            novaMatriu[x, y] = (y == values) ?randomChar(100):'0';
                        }
                    }
                    
                }
            }
            matrix.TheMatrix = novaMatriu;
        }
        private void checkKeyboard()
        {
            if (Console.KeyAvailable == true)
            {
                cki = Console.ReadKey(true);
                falling = true;
            }
        }
        private char randomChar(int percent= 25)
        {
            return (rnumber.Next(0, 100) < percent) ? (rnumber.Next(0, 100) > 49) ? characters[rnumber.Next(0, characters.GetLength(0))] : char.ToUpper(characters[rnumber.Next(0, characters.GetLength(0))]) : '0';
        }
    }
}
